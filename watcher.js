#!/usr/bin/env node

var watch = require('watch'),
    _ = require('underscore'),
    execshell = require('exec-sh'),
    path = require('path'),
    sprintf = require("sprintf-js").sprintf,
    vsprintf = require("sprintf-js").vsprintf,
    colog = require('colog'),
    program = require('commander'),
    watchCount = 0;

function Watcher(localPath, remoteInfo, options){
    this.localPath = this.stripTrailingSlash(localPath);
    this.localPath = this.asNixPath(this.localPath);
    var remoteBasePath = remoteInfo.split(":");

    if(remoteBasePath.length === 1){
        throw "incorrect format for remote: should be user@server:/path/to/base";
    }

    this.remoteServerInfo = remoteBasePath[0];
    this.remoteBasePath = this.stripTrailingSlash(remoteBasePath[1]);
    this.watchTreeOpts = {
        ignoreUnreadableDir: true,
        ignoreDotFiles: true,
        ignoreDirectoryPattern: /.git/i 
    };
}

Watcher.prototype.asNixPath = function(filepath){
    return filepath.split(path.sep).join("/");
}

Watcher.prototype.buildRsyncCmd = function(filepathToUp){
    var localRelPath = path.relative(this.localPath, filepathToUp);
    localRelPath = this.asNixPath(localRelPath);
    var remotePath = this.remoteBasePath+"/"+localRelPath;
    return sprintf("rsync -ar --exclude '.git/' %s %s:%s", filepathToUp, this.remoteServerInfo, remotePath);
};

Watcher.prototype.watch = function(){
    watch.watchTree(this.localPath, this.watchTreeOpts, function (_fileChangeInfo, curr, prev) {

        // the library changes this type - sometimes it's an object and sometimes
        // it's a string...
        // as far as i can tell, the first time it's fired is an object, which we
        // dont care about
        if(_.isString(_fileChangeInfo)){
            var fileFullpath = _fileChangeInfo;
            var cmd = this.buildRsyncCmd(fileFullpath);
            console.log(cmd);
            execshell(cmd);

            colog.success(sprintf("DONE (%d)", watchCount));
            watchCount++;
        }

    }.bind(this));
};

Watcher.prototype.stripTrailingSlash = function(filepath){
    if(filepath.substr(filepath.length-1) === "/"){
        return filepath.slice(0, filepath.length-1);
    }
    return filepath;
}

Watcher.prototype.uploadAll = function(){
    colog.info("uploading all..")
    var cmd = this.buildRsyncCmd(this.localPath+"/");
    console.log(cmd);
    execshell(cmd);
    colog.success("DONE");
};

program
    .version('0.0.1')
    .usage("<dir to watch> <user@server:/path/to/base>")
    .option('-i, --initial-sync', 'upload watched directory on start')
    .action(function(localPath, remoteInfo, _options){
        var options = {
            initialSync: _options.initialSync === undefined ? false : true
        };

        try {
            var watcher = new Watcher(localPath, remoteInfo, options);
            if(options.initialSync){
                watcher.uploadAll();
            }
            watcher.watch();    
        } catch(e){
            colog.error(e);
            process.exit();
        }
        
    })
    .parse(process.argv);
