###About
Watcher is a CLI utility which uploads files to a remote server whenever they are modified.


###Installation
Watcher is essentially a wrapper around rsync. Therefore rsync is required.

```
npm install -g
```

###Usage

```
watcher . user@server:/path/to/base/dir
```